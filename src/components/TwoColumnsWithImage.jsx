export default function TwoColumnsWithImage(props) {
  return (
    <div className="two-columns-with-image lg:flex lg:flex-wrap items-center justify-between container mx-auto pb-24">
      <div className="basis-1/2">
        <img src={props.image} alt="About NANO" className="mx-auto mb-3 max-w-screen w-lg" />
      </div>
      <div className="basis-1/2 lg:text-left lg:px-20">
        <h2 className="text-darkblue text-3xl lg:text-4xl lg:max-w-sm mx-auto mb-6 font-cerabold">
          Collaborative Intelligence for Better Healthcare
        </h2>
        <p className="lg:max-w-sm mx-auto text-base mb-2">
          Digital Healthcare Solutions for
        </p>
        <h3 className="text-darkblue text-2xl lg:text-3xl lg:max-w-sm mx-auto">
          Collaborative Intelligence for Better Healthcare
        </h3>
        <p className="lg:max-w-sm mx-auto text-base mb-5">
          Supporting government firms to facilitate critical and essential health services. We deliver mission-critical business administration solutions for government-funded healthcare plans that minimize expenses, streamline operations, enhance program support, and develop compliance while implementing automatic, easy-to-use tools and gratifying outcomes for the people and communities they serve.
        </p>
      </div>

    </div>

  )
}