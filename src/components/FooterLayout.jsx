import Logo from "../assets/Logo-main.svg"
import faceIcon from "../assets/icons/icon-sm-fb.svg"
import googleIcon from "../assets/icons/icon-sm-G+.svg"
import instaIcon from "../assets/icons/icon-sm-Insta.svg"
import linkinIcon from "../assets/icons/icon-sm-linkIn.svg"
import twitterIcon from "../assets/icons/icon-sm-twitter.svg"
const footerMenu = [
  {
    key: "about_Nano",
    link: "#",
    text: "About NANO Health Suite"
  },
  {
    key: "terms_nano",
    link: "#",
    text: "Terms & Conditions"
  },
  {
    key: "privacy_nano",
    link: "#",
    text: "Privacy Policy"
  },
  {
    key: "support_nano",
    link: "#",
    text: "Support"
  },
]
const socialIcons = [
  {
    key: "facebook_icon",
    icon: faceIcon,
    link: "https://facebook.com"
  },
  {
    key: "twitter_icon",
    icon: twitterIcon,
    link: "https://witter.com"
  },
  {
    key: "instagram_icon",
    icon: instaIcon,
    link: "https://instagram.com"
  },
  {
    key: "google_plus_icon",
    icon: googleIcon,
    link: "https://google.com"
  },
  {
    key: "linked_icon",
    icon: linkinIcon,
    link: "https://linked.com"
  },
]
export default function FooterLayout(props) {
  return (
    <div className="footer text-left">
      <div className="container mx-auto pt-20 pb-10">
        <div className="lg:grid-cols-footer md:grid-cols-2 grid gap-10  mb-6">
          <div className="grid-item">
            <img
              className="block h-[60px] w-auto mb-5"
              src={Logo}
              alt="Workflow"
            />
            <p className="text-base text-left mb-6 text-darkblue">
              An autonomous global healthcare technology corporation that intensely converges on developing data-driven analytics, accelerates innovation and advances healthcare transformation.
            </p>
            <div className="flex gap-3">
              {socialIcons.map(socialIcon => {
                return (
                  <a href={`${socialIcon.link}`} target="_blank" rel="noopener noreferrer" key={`${socialIcon.key}`} className="w-8">
                    <img src={socialIcon.icon} alt={`${socialIcon.key}`} className="w-8" />
                  </a>
                );
              })}
            </div>
          </div>
          <div className="grid-item">
            <h3 className="text-darkblue font-cerabold text-xl lg:text-2xl footer-title">
              Contact Us
            </h3>
            <p className="text-darkblue font-cerabold text-base mb-4">
              Head Office:
            </p>
            <p className="text-darkblue text-base mb-4">
              <span>312, Airport Building, Port Saeed Street, <br /> Deira, Dubai, United Arab Emirates</span>
            </p>
            <p className="text-darkblue mb-4">
              <a href="tel:+97142896454" target="_blank" rel="noopener noreferrer nofollow" > +971 (04) 2896 454 </a>
            </p>
            <p className="text-darkblue mb-4">
              <a href="mailto:sales@nanohealthsuite.com" target="_blank" rel="noopener noreferrer nofollow">sales@nanohealthsuite.com</a>
            </p>
          </div>
          <div className="grid-item">
            <h3 className="text-darkblue font-cerabold text-xl lg:text-2xl footer-title">
              Quick Links
            </h3>
            <ul>
              {footerMenu.map(menuItem => {
                return (
                  <li className="mb-4" key={`${menuItem.key}`}>
                    <a href={`${menuItem.link}`} className="text-darkblue">{menuItem.text}</a>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
        <p className="privacy-policy text-center text-sm lg:text-tiny text-gray">
          This site is protected by reCAPTCHA and the Google <a href="https://google.com" className="text-cyan-300">Privacy Policy</a> and <a href="https://google.com" className="text-cyan-300">Terms of Service</a> apply.
        </p>
      </div>
      <p className="copyright bg-cyan-500 text-white  lg:text-base py-6 text-center">
        © Copyright 2021 Nano Health Suite. All Rights Reserved.
      </p>
    </div>
  )
}