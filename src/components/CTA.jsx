export default function CTA(props) {
  return (
    <a href={props.link} type="button" className={`${props.catStyle} text-white px-4 py-2 text-base`}>
      {props.children}
    </a>
  )
}