import TitleParagraph from "../components/TitleParagraph"
import CardIcon from "../components/CardIcon"
import Insclaim from "../assets/icons/about-icons/Icon-Ins_claim.svg"
import clientsB from "../assets/icons/about-icons/Icon-B_Clients.svg"
import clientsPatient from "../assets/icons/about-icons/Icon-client-and-patient-relation.svg"
import clients from "../assets/icons/about-icons/Icon-clients.svg"

const aboutIcons = [
  {
    key: "about_icon_1",
    cardimage: Insclaim,
    cardTitle: "20 Million+",
    cardText: "Insurance Claims Processesd",
  },
  {
    key: "about_icon_2",
    cardimage: clientsPatient,
    cardTitle: "5 Million+",
    cardText: "Client and Patient Relationships",
  },
  {
    key: "about_icon_3",
    cardimage: clientsB,
    cardTitle: "30000+",
    cardText: "Business Clients Worked With",
  },
  {
    key: "about_icon_4",
    cardimage: clients,
    cardTitle: "100+",
    cardText: "Years of Collective Work Experience",
  },
]
export default function AboutSection() {
  return (
    <div className="about">
      <TitleParagraph
        subTitle="WHO WE ARE"
        mainTitle="About the Nano Health"
        text="Nano Health is devising solutions to equip better and innovative, unified healthcare that can help healthcare providers achieve better care by empowering people to make better decisions."
      />
      <div className='flex flex-wrap justify-center container mx-auto pt-6 pb-4'>
        {aboutIcons.map(aboutIcon => {
          return (
            <CardIcon
              key={aboutIcon.key}
              cardClases="w-72">
              <img src={aboutIcon.cardimage} alt={aboutIcon.cardTitle} className="mx-auto" />
              <p className='text-cyan-300 text-3xl'>{aboutIcon.cardTitle}</p>
              <p className='text-darkblue text-base'>{aboutIcon.cardText}</p>
            </CardIcon>
          );
        })}
      </div>
    </div>
  )
}