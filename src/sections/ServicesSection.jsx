import TitleParagraph from "../components/TitleParagraph"
import CardIcon from "../components/CardIcon"
import PBM from "../assets/icons/services-icons/Icon-PBM.svg"
import medicalCoding from "../assets/icons/services-icons/Icon-MedicalCoding.svg"
import IDDK from "../assets/icons/services-icons/Icon-IDDK.svg"
import DRG from "../assets/icons/services-icons/Icon-DRG.svg"
import EMR from "../assets/icons/services-icons/Icon-EMR.svg"

const servicesIcons = [
  {
    key: "service_icon_1",
    cardimage: PBM,
    cardTitle: "NANO PBM",
    cardText: "Pharmacy Benefits Management",
  },
  {
    key: "service_icon_2",
    cardimage: DRG,
    cardTitle: "NANO PBM",
    cardText: "Pharmacy Benefits Management",
  },
  {
    key: "service_icon_3",
    cardimage: EMR,
    cardTitle: "NANO PBM",
    cardText: "Pharmacy Benefits Management",
  },
  {
    key: "service_icon_4",
    cardimage: IDDK,
    cardTitle: "NANO PBM",
    cardText: "Pharmacy Benefits Management",
  },
  {
    key: "service_icon_5",
    cardimage: medicalCoding,
    cardTitle: "NANO PBM",
    cardText: "Pharmacy Benefits Management",
  },
]
export default function ServicesSection() {
  return (
    <div className="services bg-cyan-100">
      <TitleParagraph
        subTitle="DIGITAL HEALTHCARE SOLUTIONS FOR"
        mainTitle="Healthcare Service Providers"
        text="Remodeling the patient experience while encouraging efficiencies. Unifies integration and data control into a unique platform to connect data from diverse operations, applications and data sources across the entire care network."
      />
      <div className='flex flex-wrap justify-center container mx-auto mt-5 pb-24'>
        {servicesIcons.map(servicesIcon => {
          return (
            <CardIcon
              key={servicesIcon.key}
              cardClases="hover:bg-cyan-300 hover:text-white text-left hover:text-center group w-64">
              <img src={servicesIcon.cardimage} alt="about icon" className="group-hover:m-auto m-auto lg:mx-0 group-hover:invert group-hover:brightness-0" />
              <p className='text-cyan-300 lg:text-left group-hover:text-white group-hover:text-center text-lg'>{servicesIcon.cardTitle}</p>
              <p className='text-darkblue lg:text-left group-hover:text-white group-hover:text-center text-base'>{servicesIcon.cardText}</p>
            </CardIcon>
          );
        })}
      </div>
    </div>
  )
}