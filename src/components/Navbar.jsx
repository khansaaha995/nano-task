import React, { useState } from "react";
import { Bars3Icon, XMarkIcon, PhoneIcon, EnvelopeIcon, ClockIcon } from '@heroicons/react/24/outline';
import Logo from "../assets/Logo-main.svg"
import CTA from "./CTA"
const Navbar = () => {
  const [navtoggel, setNavToggel] = useState(false);
  const handelClick = () => setNavToggel(!navtoggel);
  return (
    <div className="lg:container mx-auto py-4 border border-cyan-500 lg:border-none">
      <div className="relative flex flex-row flex-wrap justify-between bg-white items-center">
        <img
          className="block h-[3rem] w-auto px-10 lg:px-0"
          src={Logo}
          alt="Workflow"
        />
        <div className={`${navtoggel ? 'absolute lg:static top-16 lg:top-0 w-full lg:w-auto left-0 bg-white p-8 text-left justify-center md:gap-4' : 'hidden'} lg:flex lg:flex-row lg:gap-y-2 lg:items-end lg:gap-5`}>
          <div className="flex justify-start mx-auto mb-4 lg:mb-0 items-start gap-2 lg:w-48 ">
            <PhoneIcon className="w-5 text-cyan-300 mt-1" />
            <div>
              <p className="text-left text-darkblue text-lg font-semibold">Call</p>
              <a href="tel:+97142896 454+" target="_blank" rel="noopener noreferrer nofollow" className="text-sm text-darkblue">+971 (04) 2896 454</a>
            </div>
          </div>
          <div className="flex justify-start mx-auto mb-4 lg:mb-0 items-start gap-2 lg:w-52 ">
            <EnvelopeIcon className="w-5 text-cyan-300 mt-1" />
            <div>
              <p className="text-left text-darkblue text-lg font-semibold">Connect with us</p>
              <a href="mailto:sales@nanohealthsuite.com" target="_blank" rel="noopener noreferrer nofollow" className="text-sm text-darkblue">sales@nanohealthsuite.com</a>
            </div>
          </div>
          <div className="flex justify-start mx-auto mb-4 lg:mb-0 items-start gap-2 lg:w-48 ">
            <ClockIcon className="w-5 text-cyan-300 mt-1" />
            <div>
              <p className="text-left text-darkblue text-lg font-semibold">Sat - Thu</p>
              <p  className="text-sm text-darkblue">07:00 - 22:00</p>
            </div>
          </div>
          {/* <div className={`${navtoggel ? 'absolute top-0 right-12' : ''} `}> */}
          <CTA link="#" catStyle="bg-cyan-300">
            Contact Us
          </CTA>
          {/* </div> */}
        </div>
        <div className="lg:hidden md:flex pr-10 lg:px-0">
          {navtoggel ?
            <XMarkIcon onClick={handelClick} className="w-7" /> :
            <Bars3Icon onClick={handelClick} className="w-7" />}
        </div>
      </div>
    </div>
  );
}

export default Navbar