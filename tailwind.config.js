/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    colors: {
      cyan: {
        100 : "#f2f7fa",
        200: "#d2f7fc",
        300: "#06b6d4",
        400: "#4ebece",
        500: "#1d829b",
        600: "#b8d5e0"
        
      },
      darkblue: "#16375B",
      white: "#fff",
      gray: "#6F6F6F"

    },
    borderRadius: {
      DEFAULT: '0.5rem',
      DEFAULT: '6px',
      20: "16px"
    },
    fontSize: {
      'xs': '.75rem',
      'sm': '.8rem',
      'tiny': '.875rem',
      'base': '1rem',
      'lg': '1.125rem',
      'xl': '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '4.5xl': '2.5rem',
      '5xl': '3rem',
      '6xl': '4rem',
      '7xl': '5rem',
    },
    extend: {
      boxShadow: {
        '3xl': '1px 1px 20px #ccc',
      },
      fontFamily: {
        ceralight: ['cera-light'],
        cerameduim: ['cera-meduim'],
        cerabold: ['cera-bold'],
      },
      gridTemplateColumns: {
        'footer': '45% 25% 30%',
      }
    },
  },
}