export default function TitleParagraph(props) {
  return (
    <div className="title-paragraph container mx-auto pt-24">
      <h3 className="bg-cyan-200 inline-block text-cyan-400 py-3 px-7 mb-4 rounded text-sm font-cerabold">
        {props.subTitle}
      </h3>
      <h2 className=" text-darkblue text-3xl md:text-4xl lg:text-4.5xl max-w-4xl mx-auto font-cerabold">
        {props.mainTitle}
      </h2>
      <p className="max-w-4xl mx-auto text-base mb-5">
        {props.text}
      </p>
    </div>
  )
}