export default function SubscriptionForm() {
  return (
    <div className="bg-cyan-600 py-24">
      <div className="container mx-auto">
        <h2 className="color-darkblue text-2xl lg:text-3xl font-bold mb-5">Subscribe Our Newsletter</h2>
        <form className="w-full max-w-lg mx-auto">
          <div className="flex items-center">
            <input type="email" id="email" className="bg-gray-50 text-gray-900 text-base rounded-l block w-96 px-2 py-3 placeholder:font-ceralight focus:outline-none"
              placeholder="Enter your email address" required />
            <button className="bg-cyan-500 hover:bg-cyan-400 rounded-r hover:border-teal-700 text-base text-white py-3 px-5" type="button">
              Subscribe
            </button>
          </div>
        </form>
      </div>
    </div>
  )
}