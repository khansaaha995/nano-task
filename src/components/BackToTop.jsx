import React, { useState } from 'react';
import backToTopImage from "../assets/bacltotop.png"

const scrollTop = () => {
  window.scrollTo({ top: 0, behavior: 'smooth' });
};

export default function BackToTop() {
  const [showScroll, setShowScroll] = useState(false)
  const checkScrollTop = () => {
    if (!showScroll && window.pageYOffset > 300) {
      setShowScroll(true)
    } else if (showScroll && window.pageYOffset <= 300) {
      setShowScroll(false)
    }
  };
  window.addEventListener('scroll', checkScrollTop)
  return (
    <img src={backToTopImage}
      onClick={scrollTop}
      alt="back to top icon"
      className={`${showScroll ? 'flex' : 'hidden'} w-10 fixed right-10 bottom-24 opacity-50 hover:opacity-80`}/>
  )
}