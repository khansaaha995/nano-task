import './App.css';
import Navbar from "./components/Navbar"
import Banner from "./components/Banner"
import TwoColumnsWithImage from "./components/TwoColumnsWithImage"
import aboutImage from "./assets/images/about-image.svg"
import SubscriptionForm from "./components/SubscriptionForm"
import BackToTop from "./components/BackToTop"
import FooterLayout from "./components/FooterLayout"
import AboutSection from "./sections/AboutSection"
import ServicesSection from "./sections/ServicesSection"

function App() {
  return (
    <div className="App">
      <header>
        <Navbar />
      </header>
      <main>
        <Banner />
        <AboutSection />
        <TwoColumnsWithImage image={aboutImage} />
        <ServicesSection />
        <SubscriptionForm />
      </main>
      <footer>
        <FooterLayout />
        <BackToTop />
      </footer>
    </div>
  );
}

export default App;
