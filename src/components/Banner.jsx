import CTA from "./CTA"
export default function Banner() {
  return (
    <div className="w-screen py-32 bg-gradient-to-r from-cyan-300 to-cyan-500 flex items-center">
      <div className="container mx-auto  ">
        <h1 className="text-4xl lg:text-5xl text-white font-bold max-w-4xl mx-auto mb-5 font-cerabold">
          100% Automation in Healthcare Operations and Services Such as Claims, Approvals and More...
        </h1>
        <p className="text-xl text-white max-w-4xl mx-auto mb-10">
          We help deliver better solutions for payers, hospitals, doctors, pharmacies, health plans, governments, employers and the millions of lives they touch.
        </p>
        <CTA link="#" catStyle="bg-cyan-400 rounded px-10 py-5">
          REQUEST A DEMO
        </CTA>
      </div>
    </div>
  )
}