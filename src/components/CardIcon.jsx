export default function CardIcon(props) {
  return (
    <div className={`${props.cardClases} text-center px-14 py-5 rounded-2xl hover:shadow-3xl shadow-black mb-7 rounded-20`}>
      {props.children}
    </div>
  )
}